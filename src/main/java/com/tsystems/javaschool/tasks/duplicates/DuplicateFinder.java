package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.*;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        
        if (sourceFile == null || targetFile == null) {
           throw new IllegalArgumentException();
        }        
        List<String> list;
        try {
            list = Files.readAllLines(Paths.get(sourceFile.toURI()));
        } 
        catch (NoSuchFileException | FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
        catch (IOException e) {
            throw new IllegalStateException(e);
        }

        Map<String, Integer> map = new TreeMap<>(); 
        
        for(String s : list) {
            map.put(s, Collections.frequency(list, s));
        }
        FileWriter out = null;
        try{
            out = new FileWriter(targetFile, true);             
            for (Map.Entry<String, Integer> entry : map.entrySet())
                out.write(entry.getKey() + "[" + entry.getValue() + "]" + "\n");
        }
        catch (IOException e) {
            throw new IllegalStateException(e);
        }
        finally{
            if(out != null)
                try {
                    out.close();
                } 
                catch(IOException e) {
                    throw new IllegalStateException(e);
                }
        }        
        return true;
    }


}
