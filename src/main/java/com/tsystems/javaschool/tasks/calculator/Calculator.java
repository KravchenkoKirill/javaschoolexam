package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if(checkStatement(statement))
            return null;
        LinkedList<Double> operands = new LinkedList<>();
        LinkedList<Character> operators = new LinkedList<>();
        try{
            for(int i = 0; i < statement.length(); i++){
                char c = statement.charAt(i);
                if(c == '(')
                    operators.add(c);
                else if(c == ')'){
                    while (operators.getLast() != '('){
                        calculate(operands, operators.removeLast());
                    }
                    operators.removeLast();
                }
                else if(c == '+' || c == '-' || c == '*' || c == '/' ){
                    if(!operators.isEmpty() && selectPriority(operators.getLast()) >= selectPriority(c))
                        calculate(operands, operators.removeLast());
                    operators.add(c);
                }
                else{                    
                    String s = "";
                    while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')){
                        s += statement.charAt(i++);
                    }
                    --i;
                    operands.add(Double.parseDouble(s));                    
                }
            }
            while(!operators.isEmpty())
                calculate(operands, operators.removeLast());                  
        }
        catch(NumberFormatException | ArithmeticException | UnsupportedOperationException e){
            return null;
        }
        double result = operands.getLast();
        if (result % 1 == 0)
            return format(result);
        else
            return format(round(result));
    }
    
    
    private boolean checkStatement(String statement){
        if(statement == null ||
                statement.isEmpty() ||
                statement.contains("++") ||
                statement.contains("--") ||
                statement.contains("**") ||
                statement.contains("//") ||
                statement.contains(".."))
            return true;
        int parenthesescount = 0;
        for(char c : statement.toCharArray()){
            if(c == '(')
                parenthesescount++;
            else if(c == ')'){
                parenthesescount--;
                if(parenthesescount < 0)
                    return true;             
            }
        }
        if(parenthesescount != 0)
            return true;
        return false;
    }
    
    private int selectPriority(char c){
        if (c == '*' || c == '/'){
            return 1;
        } 
        else if (c == '+' || c == '-'){
            return 0;
        }
        else{
            return -1;
        }
    }
    
    private void calculate(LinkedList<Double> operands, char operator){  
        double oper1 = operands.removeLast();
        double oper2 = operands.removeLast();
        switch(operator){
            case '+':
                operands.add(oper1 + oper2);
                break;
            case '-':
                operands.add(oper2 - oper1);
                break;
            case '*':
                operands.add(oper1 * oper2);
                break;
            case '/':
                if(oper1 == 0)
                    throw new ArithmeticException();
                else
                    operands.add(oper2 / oper1);
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }
    
    private String format(double d){               
        if(d == (long) d)
            return String.format(Locale.US, "%d", (long) d);
        else
            return String.format(Locale.US, "%s", d);
    }

    private double round(double value){        
        value *= 10000;
        long tmp = Math.round(value);
        return (double) tmp / 10000;
    }

}
